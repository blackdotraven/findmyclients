"""
An example of a Caribou migration file.
"""


def upgrade(connection):
    # connection is a plain old sqlite3 database connection
    sql = """
        CREATE TABLE clients (
            token    TEXT UNIQUE
                          NOT NULL,
            hostname TEXT,
            ip       TEXT,
            updated  TEXT,
            version  TEXT,
            data     TEXT
        );
    """
    connection.execute(sql)
    connection.commit()


def downgrade(connection):
    return
