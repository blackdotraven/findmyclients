#!/bin/bash
TOKEN="ADD_YOUR_TOKEN"
VERSION="1.0.0"
URL="http://127.0.0.1:8080/"


read -r -d '' data << EOF
--------------
-- Hostname --
--------------

$(hostname -f)

--------
-- IP --
--------

$(ip addr)
EOF

data=$(echo "$data" | base64 | tr -d '\n')

curl -H "Content-Type: application/json" -d "{\"data\": \"$data\", \"token\": \"$TOKEN\", \"hostname\": \"$(hostname -f)\", \"version\": \"$VERSION\"}" $URL
