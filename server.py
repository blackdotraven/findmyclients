#!/usr/bin/env python3
import pprint
from http.server import BaseHTTPRequestHandler, HTTPServer
import logging
import json
import sqlite3
import base64


class S(BaseHTTPRequestHandler):

    def do_GET(self):

        if self.path == '/admin/viewstuff':

            con = sqlite3.connect('database.db')
            cur = con.cursor()
            cur.execute('select hostname, ip, updated, version, data from clients order by hostname asc')

            rows = cur.fetchall()

            response_text = ''

            response_text += '''
                <style>
                    table, td, th {
                      border: 1px solid;
                    }
                    
                    table {
                      width: 100%;
                      border-collapse: collapse;
                    }
                </style>
                <table>
                    <thead>
                        <tr>
                            <th>Hostname</th>
                            <th>IP</th>
                            <th>Updated</th>
                            <th>Version</th>
                            <th>Data</th>
                        </tr>
                    </thead>
            '''

            # skip headers
            rows.reverse()
            rows.pop()
            rows.reverse()

            for row in rows:
                response_text += '''
                    <tr>
                        <th>{}</th>
                        <th>{}</th>
                        <th>{}</th>
                        <th>{}</th>
                        <th><pre>{}</pre></th>
                    </tr>
                '''.format(row[0], row[1], row[2], row[3], row[4].decode('utf-8'))

            response_text += '''
                </table>
            '''

            self.send_response(200)
            self.end_headers()
            self.wfile.write(response_text.encode('utf-8'))

        else:
            self.send_response(404)
            self.send_header('Content-type', 'text/html')
            self.end_headers()


    def do_POST(self):
        response_code = 500
        response_text = ''

        content_length = int(self.headers['Content-Length'])
        post_data = self.rfile.read(content_length).decode('utf-8')

        if self.headers['Content-Type'] == 'application/json':

            try:
                json_body = json.loads(post_data)
                data = 'n/A'
                hostname = 'n/A'
                version = 'n/A'
                ip = self.client_address[0]

                if 'data' in json_body:
                    data = base64.b64decode(json_body['data'])

                if 'hostname' in json_body:
                    hostname = json_body['hostname']

                if 'version' in json_body:
                    version = json_body['version']

                if 'token' in json_body:
                    token = json_body['token']

                    con = sqlite3.connect('database.db')
                    cur = con.cursor()
                    cur.execute('select * from clients where token = ?', (token,))

                    rows = cur.fetchall()

                    if len(rows) > 0:
                        sql_ret = cur.execute('update clients set hostname = ?, ip = ?, data = ?, updated = ?, version = ? where token = ?', (hostname, ip, data, self.date_time_string(), version, token))
                        con.commit()

                        if sql_ret.rowcount > 0:
                            response_code = 200
                            response_text += 'data updated\n'
                        else:
                            response_code = 500
                            response_text += 'update failed\n'

                    else:
                        response_code = 403
                        response_text += 'invalid token\n'
                else:
                    response_code = 403
                    response_text += 'no token found\n'

            except ValueError as err:
                response_text += 'invalid JSON or processing exception\n'
                response_code = 400

        else:
            response_code = 500
            response_text = 'wrong Content-Type'

        self.send_response(response_code)
        self.end_headers()
        self.wfile.write(response_text.encode('utf-8'))


def run(server_class=HTTPServer, handler_class=S, port=8888):
    logging.basicConfig(level=logging.INFO)
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    logging.info('Starting httpd...\n')
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    httpd.server_close()
    logging.info('Stopping httpd...\n')


if __name__ == '__main__':
    from sys import argv
    import caribou

    caribou.upgrade("database.db", "migrations/")

    if len(argv) == 2:
        run(port=int(argv[1]))
    else:
        run()
