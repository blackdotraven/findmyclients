#!/usr//bin/env bash

echo ""
echo "== preparing venv =="
echo ""
python3 -mvenv venv
source venv/bin/activate
pip3 install -r requirements.txt

python3 server.py
