# findMyClients

## Overview

FindMyClients consists of a small Python web server and a client-side script. Its primary purpose is to send IP information to the web server.

The use case for this system is when multiple clients are deployed for short durations on a trips. In such scenarios, knowing the assigned DHCP-IP is crucial for establishing SSH connections. The client script is scheduled to run via a Cronjob every minute, sending IP address data to an HTTPS endpoint. This data is then stored in an SQLite database.

## Scripts/Files

- `add_client.sh`: Used to generate a token.
- `client.sh`: Responsible for transmitting data to the server.
- `server.py`: Creates the HTTP web server.
- `server.sh`: Loads the virtual environment (venv) and starts `server.py`.
- `show_clients.sh`: Prints client information.

## Architecture

```
  +-----------+          +------------------+              +-----------+             
  |           |   HTTPS  |                  |     HTTP     |           |             
  | client.sh |----------| ReverseProxy     |--------------| server.py |             
  |           |          | (bring your own) |              |           |             
  +-----------+          |                  |              +-----------+             
                         +------------------+                           
```

## Responsibilities
- `client.sh`: This script sends data and a token in an HTTPS request.
- `ReverseProxy`: Handles HTTP requests and likely performs some magic.
- `server.py`: Receives HTTP POST requests with tokens and updates corresponding database entries.
